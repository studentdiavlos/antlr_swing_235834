tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer actual_scope = 1;
}
prog    : (e+=zakres | e+=expr | d+=decl)* -> program(name={$e},declarations={$d})
    ;

zakres   :
        ^(BEGBLOCK {actual_scope = locals.enterScope();} (e+=zakres | e+=expr | d+=decl)*) {actual_scope = locals.leaveScope();} -> block(expressions={$e}, declarations={$d})
    ;

decl    :
        ^(VAR i1=ID) {locals.newSymbol($ID.text);} -> declare(n={locals.getSymbolNameWithNumber($ID.text)})
    ;
        catch [RuntimeException ex] {errorID(ex,$i1);}

expr    : ^(PLUS  e1=expr e2=expr) -> add(p1={$e1.st},p2={$e2.st})
        | ^(MINUS e1=expr e2=expr) -> subtract(p1={$e1.st},p2={$e2.st})
        | ^(MUL   e1=expr e2=expr) -> multiply(p1={$e1.st},p2={$e2.st})
        | ^(DIV   e1=expr e2=expr) -> divide(p1={$e1.st},p2={$e2.st})
        | ^(PODST i1=ID   e2=expr) {locals.hasSymbol($ID.text);} -> assign(p1={locals.getSymbolNameWithNumber($ID.text)},p2={$e2.st})
        | INT                      -> int(i={$INT.text})
        | ID                       {locals.getSymbol($ID.text);} -> id(i={locals.getSymbolNameWithNumber($ID.text)})
    ;
    